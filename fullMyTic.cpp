#include "fullMyTic.h"
#include "user.h"

FullMyTic::FullMyTic() {}

FullMyTic::FullMyTic(User* user) : MyTic(user) {}

FullMyTic::FullMyTic(float credit, User* user) : MyTic(credit, user) {}

FullMyTic::~FullMyTic() {}

float FullMyTic::getCostOf(TravelPass& travelPass) const {
    return travelPass.getCost();
}