#include "twoHoursZone1And2.h"

const string TwoHoursZone1And2::PASS_LENGTH = "2 Hour";
const string TwoHoursZone1And2::PASS_ZONES = "Zone 1 and 2";
const float TwoHoursZone1And2::PASS_COST = 3.50f;

TwoHoursZone1And2::TwoHoursZone1And2() 
    : TravelPass(PASS_LENGTH, PASS_ZONES, PASS_COST) {}

TwoHoursZone1And2::TwoHoursZone1And2
(string theLength, string theZones, float theCost) 
    : TravelPass(theLength, theZones, theCost) {}

TwoHoursZone1And2::~TwoHoursZone1And2() {}

TravelPass* TwoHoursZone1And2::clone() const {
    return new TwoHoursZone1And2(*this);
}

int TwoHoursZone1And2::getExpiry() const {
    return getTimeOfPurchase() + 200;
}

bool TwoHoursZone1And2::isBestMatch(Journey& journey) {
    return (journey.getDuration() <= 2 && journey.getZone() == 2);
}

bool TwoHoursZone1And2::isValid(Journey& journey) {
    return (journey.getDay() == journeys.front()->getDay()
        && journey.getArriveTime() <= journeys.front()->getDepartTime() + 200);
}

bool TwoHoursZone1And2::isTravelPass(const TravelPass& travelPass) {
    return (typeid(*this) == typeid(travelPass));
}

void TwoHoursZone1And2::input() {
    cin >> *this;
}

void TwoHoursZone1And2::print() {
    cout << *this;
}