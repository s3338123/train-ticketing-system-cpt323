#include "chargeCardCommand.h"

ChargeCardCommand::ChargeCardCommand(MyTicSystem &system) : system(system) {}

ChargeCardCommand::~ChargeCardCommand() {}

int ChargeCardCommand::execute() {
    system.rechargeCard();
    return 1;
}