#include "stationRegister.h"

map<string, int> StationRegister::started;
map<string, int> StationRegister::ended;

StationRegister::StationRegister() {}

StationRegister::~StationRegister() {}

void StationRegister::start(const Station& station) {
    try {
        started.at(station.getName())++;
    }
    catch (std::out_of_range) {
        started[station.getName()] = 1;
    }
}

void StationRegister::end(const Station& station) {
    try {
        ended.at(station.getName())++;
    }
    catch (std::out_of_range) {
        ended[station.getName()] = 1;
    }
}

int StationRegister::getStarted(const Station& station) {
    try {
        return started.at(station.getName());
    }
    catch (std::out_of_range) {
        return 0;
    }
}

int StationRegister::getEnded(const Station& station) {
    try {
        return ended.at(station.getName());
    }
    catch (std::out_of_range) {
        return 0;
    }
}