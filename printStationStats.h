#ifndef __PRINTSTATIONSTATS_H__
#define __PRINTSTATIONSTATS_H__

#include <iostream>

#include "function.h"
#include "station.h"
#include "stationRegister.h"

using std::cout;

class PrintStationStats : public Function < std::pair<string, Station*>, 
    Station* > {
public:
    PrintStationStats();
    ~PrintStationStats();

    virtual Station* operator() (std::pair<string, Station*> pair);
};

#endif