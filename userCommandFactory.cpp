#include "userCommandFactory.h"

UserCommandFactory::UserCommandFactory(MyTicSystem& system) : system(system) {
    this->commands["addNewUser"] = new AddNewUserCommand(system);
    this->commands["buyJourney"] = new BuyJourneyCommand(system);
    this->commands["chargeCard"] = new ChargeCardCommand(system);
    this->commands["printUserReports"] = new PrintUserReportsCommand(system);
    this->commands["saveQuit"] = new SaveQuitCommand(system);
    this->commands["showStationStats"] = new ShowStationStatsCommand(system);
    this->commands["showUserCredit"] = new ShowCreditCommand(system);
    this->commands["updatePricing"] = new UpdatePricingCommand(system);
    this->commands["nullCommand"] = new NullCommand;
}

UserCommandFactory::~UserCommandFactory() {
    for (auto& command : commands) {
        if (command.second) {
            delete command.second;
            command.second = NULL;
        }
    }
    commands.clear();
}

UserCommand* UserCommandFactory::make(string command) {
    try {
        return this->commands.at(command);
    }
    catch (std::out_of_range) {
        return commands.at("nullCommand");
    }
}