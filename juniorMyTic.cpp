#include "juniorMyTic.h"

const float JuniorMyTic::DISCOUNT_RATE = 0.5f;

JuniorMyTic::JuniorMyTic() {}

JuniorMyTic::JuniorMyTic(User* user) : MyTic(user) {}

JuniorMyTic::JuniorMyTic(float credit, User* user) : MyTic(credit, user) {}

JuniorMyTic::~JuniorMyTic() {}

void JuniorMyTic::buyPass(TravelPass& travelPass, Journey& journey, 
    bool printOut) {
        travelPass.setCost(getCostOf(travelPass));
        MyTic::buyPass(travelPass, journey, printOut);
}

float JuniorMyTic::getCostOf(TravelPass& travelPass) const {
    return travelPass.getCost() * DISCOUNT_RATE;
}