#ifndef __RUNVISITOR_H__
#define __RUNVISITOR_H__

#include "menuNavigator.h"
#include "visitor.h"

/*
Allows execution of different functions based upon the object type being dealt 
with. Used here specifically to decide what action to take when encountering 
either a Menu or a Menu Item
 */
class RunVisitor : public Visitor {
public:
	RunVisitor(MenuNavigator& navigator);
    ~RunVisitor();

	virtual void visit(Menu& item) const;
	virtual void visit(MenuItem& item) const;
private:
    // Make sure we are storing the address of the original navigator object.
    // Changes to navigator need to be visible to the calling function.
    MenuNavigator& navigator;
};


#endif
