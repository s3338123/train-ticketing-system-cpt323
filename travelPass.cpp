#include "travelPass.h"

TravelPass::TravelPass() {}

TravelPass::TravelPass(string theLength, string theZones, float theCost) 
    : length(theLength), zones(theZones), cost(theCost) {}

TravelPass::~TravelPass() {
    for (auto& journey : journeys) {
        if (journey) {
            delete journey;
            journey = NULL;
        }
    }
    journeys.clear();
}

istream& operator>>(istream& input, TravelPass& travelPass) {
    string length;
    string zones;
    float cost;

    // Rather than passing input directly to travelPass.attribute, store them
    // locally so that we can pass them to the relavent set() functions on
    // travelPass, as the set functions may decide to perform some validation
    getline(input, length, ':');
    getline(input, zones, ':') >> cost;

    travelPass.setLength(length);
    travelPass.setZones(zones);
    travelPass.setCost(cost);

    return input;
}

ostream& operator<<(ostream& output, const TravelPass& travelPass) {
    output << travelPass.getLength() << " " << travelPass.getZones() 
        << " Travel Pass purchased";
    return output;
}

float TravelPass::getCost() const {
    return cost;
}

Journey::Day TravelPass::getDayOfPurchase() const {
    return journeys.front()->getDay();
}

vector<Journey*>& TravelPass::getJourneys() {
    return journeys;
}

string TravelPass::getLength() const {
    return length;
}

int TravelPass::getTimeOfPurchase() const {
    return journeys.front()->getDepartTime();
}

string TravelPass::getZones() const {
    return zones;
}

void TravelPass::addJourney(Journey& journey) {
    journeys.push_back(&journey);
}

void TravelPass::swapJourneys(vector<Journey*>& copy) {
    journeys.swap(copy);
}

/*
Tests if this travel pass has not expired and is eligible to purchase the 
journey.
*/
bool TravelPass::canBuy(Journey& journey) {
    // Make sure we aren't travelling back in time to make the new journey
    if (!journeys.empty()) {
        Journey::Day prevDay = journeys.back()->getDay();
        Journey::Day currDay = journey.getDay();

        // If days don't match, journey doesn't fit on pass, so exit
        if (prevDay != currDay) {
            return false;
        }

        int prevArrivalTime = journeys.back()->getArriveTime();
        int currentDepartTime = journey.getDepartTime();

        if (currentDepartTime < prevArrivalTime) {
            throw TravelPass::TimeTravelException
                ("Can not travel back in time to make journey");
        }
    }
    else {
        return isBestMatch(journey);
    }

    return (isValid(journey) && isBestMatch(journey));
}

void TravelPass::setLength(string newLength) {
    length = newLength;
}

void TravelPass::setZones(string newZones) {
    zones = newZones;
}

void TravelPass::setCost(float newCost) {
    cost = newCost;
}