#ifndef __UPDATEPRICINGCOMMAND_H__
#define __UPDATEPRICINGCOMMAND_H__

#include "userCommand.h"
#include "myTicSystem.h"

class UpdatePricingCommand : public UserCommand {
public:
    UpdatePricingCommand();
    UpdatePricingCommand(MyTicSystem& system);
    ~UpdatePricingCommand();

    virtual int execute();
private:
    MyTicSystem system;
};

#endif