#include "utility1.h"

////////////////////////////////////////////
// function to clear the input buffer
void clearInputBuffer() {
	char symbol;
	do {
		cin.get(symbol);
	}
	while(symbol != '\n');
}
