#include "myTicSystem.h"

MyTicSystem::MyTicSystem(map<string, User*>& users, set<TravelPass*>& passes, 
    map<string, Station*>& stations)
        : users(users), priceList(passes), stations(stations) {}

MyTicSystem::~MyTicSystem() {}

void MyTicSystem::buyJourney() {
    string userId;
    cout << "Which user: "; cin >> userId;
    try { users.at(userId); }
    catch (std::out_of_range) {
        cerr << "Invalid user\n";
        return;
    }

    Journey* journey = NULL;
    try { journey = getJourneyDetails(); }
    catch (Journey::JourneyException e) {
        cerr << e.message << endl;
        if (journey) {
            delete journey;
            journey = NULL;
        }
        return;
    }

    MyTic *myTic = users.at(userId)->getMyTic();
    TravelPass* lastPass = myTic->getLastPurchase();
    TravelPass* upgradePass = BestUpgrade(*journey)(priceList);
    TravelPass* newPass = BestMatch(*journey)(priceList);

    try {
        // Check if the previously purchased travel pass can be used to cover 
        // this journey
        if (lastPass && lastPass->canBuy(*journey)) {
            delete newPass;
            delete upgradePass;
            newPass = NULL;
            upgradePass = NULL;
            myTic->buyJourney(*lastPass, *journey);
        }
        // If not, check to see if we can upgrade the users travel pass to a 
        // cheaper option
        else if (lastPass && newPass && upgradePass &&
            myTic->canUpgrade(*journey, *lastPass, *newPass, *upgradePass)) {
            delete newPass;
            newPass = NULL;
            myTic->buyUpgrade(*upgradePass, *journey);
        }
        else if (newPass) { // Failing the above, buy a new pass
            delete upgradePass;
            upgradePass = NULL;
            myTic->buyPass(*newPass, *journey);
        }
        else {
            // No pass to purchase
        }
    }
    catch (const TravelPass::TravelPassException& e) {
        cerr << e.message << endl;
        if (newPass) {
            delete newPass;
            newPass = NULL;
        }
        if (upgradePass) {
            delete upgradePass;
            upgradePass = NULL;
        }
        if (journey) {
            delete journey;
            journey = NULL;
        }
    }
    catch (const MyTic::MyTicException& e) {
        cerr << e.message << endl;
        if (newPass) {
            delete newPass;
            newPass = NULL;
        }
        if (upgradePass) {
            delete upgradePass;
            upgradePass = NULL;
        }
        if (journey) {
            delete journey;
            journey = NULL;
        }
    }
}

void MyTicSystem::rechargeCard() {
    string userId;
    cout << "Which user: "; cin >> userId;
    try { users.at(userId); }
    catch (std::out_of_range) {
        cerr << "Invalid user\n";
        return;
    }

    int amount;
    cout << "Amount: "; cin >> amount;
    
    MyTic *myTic = users.at(userId)->getMyTic();
    try { myTic->addCredit(amount); }
    catch (const MyTic::MyTicException& e) { cerr << e.message << endl; }

    cout << "Credit remaining for " << userId << ": $" << myTic->getCredit() 
        << endl;
}

void MyTicSystem::printUserReports() {
    cout << "\nUser reports: \n";
    for (auto& it : users) {
        string userId = it.first;
        User& user = *it.second;
        cout << "Purchases by " << userId << ":\n";
        user.getMyTic()->printPurchases();
    }
}

void MyTicSystem::showUserCredit() {
    string userId;
    cout << "Which user: "; cin >> userId;
    try { users.at(userId); }
    catch (std::out_of_range) {
        cerr << "Invalid user\n";
        return;
    }

    users.at(userId)->getMyTic()->print();
}

void MyTicSystem::updatePricing() {
    string duration;
    string zone;
    float price;
    char durationChoice;
    char zoneChoice;

    do {
        cout << "Which duration pass do you want to update: (2)Hour or (A)ll "
            << "Day: ";
        cin >> durationChoice;
        if (durationChoice == '2') {
            duration = "2Hour";
        }
        else if (toupper(durationChoice) == 'A') {
            duration = "AllDay";
        }
        else {
            cout << "Invalid choice\n";
            clearInputBuffer();
        }
    } while (!(durationChoice == '2' || durationChoice == 'A'));
    clearInputBuffer();

    do {
        cout << "Which Zone do you want to update: Zone (1) or Zone 1-(2): ";
        cin >> zoneChoice;
        if (zoneChoice == '1') {
            zone = "Zone1";
        }
        else if (zoneChoice == '2') {
            zone = "Zone12";
        }
        else {
            cout << "Invalid choice\n";
            clearInputBuffer();
        }
    } while (!(zoneChoice == '1' || zoneChoice == '2'));
    
    clearInputBuffer();
    cout << "What is the new price: "; cin >> price;

    TravelPass *temp = NULL;
    try {
        stringstream ss(duration + ':' + zone);
        temp = TravelPassFactory::make(ss);
    }
    catch (const TravelPassFactory::InvalidTravelPassException e) {
        cerr << e.message << endl;
        if (temp) {
            delete temp;
            temp = NULL;
        }
        return;
    }

    for (auto& pass : priceList) {
        if (temp->isTravelPass(*pass)) {
            pass->setCost(price);
        }
    }

    cout << "New price of " << duration << " " << zone << " Travel Pass is $" 
        << price << endl;
    if (temp) {
        delete temp;
        temp = NULL;
    }
}

void MyTicSystem::showStationStats() {
    cout << "\nStation travel statistics:\n";
    for_each(stations.begin(), stations.end(), PrintStationStats());
}

void MyTicSystem::addNewUser() {
    string userId;
    string name;
    string email;
    string userType;
    char type;
    stringstream ss;

    cout << "Choose an ID: "; getline(cin, userId);
    try { users.at(userId); }
    catch (std::out_of_range) {
        cout << "User name: "; getline(cin, name);
        cout << "User email: "; getline(cin,email);
        do {
            cout << "Choose user type: (a)dult, (j)unior, (s)enior? ";
            cin >> type;
            type = tolower(type);
            if (type == 'a') {
                userType = "adult";
            }
            else if (type == 'j') {
                userType = "junior";
            }
            else if (type == 's') {
                userType = "senior";
            }
            else {
                cout << "Invalid user type\n";
                clearInputBuffer();
            }
        } while (!(type == 'a' || type == 'j' || type == 's'));


        try {
            ss.clear();
            ss.str(userId + ':' + userType + ':' + name + ':' + email 
                + ':' + "0");
            users[userId] = UserFactory::make(ss);
            //users.insert(std::pair<string, User*>(userId, user));
            cout << endl << "Created new " << userType << " user with id " 
                << userId << endl;
        }
        catch (const UserFactory::InvalidUserTypeException& e) {
            cerr << e.message << endl;
        }

        return;
    }

    cerr << "Sorry, that id is already in use.\n";
}

/*
Iterates the users and passes containers and outputs their contents to the 
output stream
*/
void MyTicSystem::saveTo(ostream& output) {
    output << "#users   (last field is initial credit)\n";
    for (auto& it : users) {
        output << *it.second << endl;
    }
    output << "#prices\n";
    for (auto& pass : priceList) {
        output << pass->getLength() << ':' << pass->getZones() << ':'
            << std::fixed << std::showpoint << std::setprecision(2)
            << pass->getCost() << endl;
    }
}

Journey* MyTicSystem::getJourneyDetails() {
    string startStation, endStation, day;
    int departTime, arriveTime;

    cout << "From what station: "; cin >> startStation;
    Station start;
    try { start = *stations.at(startStation); }
    catch (std::out_of_range) {
        throw Journey::InvalidStationException
            ("Departure station does not exist");
    }

    cout << "To what station: "; cin >> endStation;
    Station end;
    try { end = *stations.at(endStation); }
    catch (std::out_of_range) {
        throw Journey::InvalidStationException
            ("Arrival station does not exist");
    }

    cout << "What day: "; cin >> day;
    if (!(day == "mon" || day == "tue" || day == "wed" || day == "thu" 
        || day == "fri" || day == "sat" || day == "sun")) {
        throw Journey::InvalidDayException
            ("Invalid day. Must be in three letter day format, eg. 'mon'");
    }

    cout << "Departure time: "; cin >> departTime;
    if (departTime < 0 || departTime > 2359) {
        throw Journey::InvalidTimeException("Invalid departure time");
    }

    cout << "Arrival time: "; cin >> arriveTime;
    if (arriveTime < 0 || arriveTime > 2359) {
        throw Journey::InvalidTimeException("Invalid arrival time");
    }

    return new Journey(start, end, day, departTime, arriveTime);
}
