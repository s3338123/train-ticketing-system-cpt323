#include "saveQuitCommand.h"

SaveQuitCommand::SaveQuitCommand(MyTicSystem& system) : system(system) {}

SaveQuitCommand::~SaveQuitCommand() {}

int SaveQuitCommand::execute() {
    //system.save();
    return 0;
}