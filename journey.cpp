#include "journey.h"

Journey::Journey() {}

Journey::Journey(Station& start, Station& end, string day, int departTime, 
    int arriveTime)
        : start(start), end(end), day(setDay(day)), departTime(departTime), 
        arriveTime(arriveTime), 
        duration(setDuration()), zone(setZone()) {}

Journey::~Journey() {}

Journey::Day Journey::getDay() const {
    return day;
}

float Journey::getDuration() const {
    return duration;
}

int Journey::getZone() const {
    return zone;
}

Station Journey::getDepartStation() const {
    return start;
}

int Journey::getDepartTime() const {
    return departTime;
}

Station Journey::getArriveStation() const {
    return end;
}

int Journey::getArriveTime() const {
    return arriveTime;
}

Journey::Day Journey::setDay(string day) {
    if (day == "sun") return Day::sun;
    else if (day == "mon") return Day::mon;
    else if (day == "tue") return Day::tue;
    else if (day == "wed") return Day::wed;
    else if (day == "thu") return Day::thu;
    else if (day == "fri") return Day::fri;
    else if (day == "sat") return Day::sat;
    else throw Journey::InvalidDayException("Invalid day: " + day 
        + ". Must be in three letter day format, eg. 'mon'");
}

float Journey::setDuration() {
    // If the arriveTime is less than departTime, then we've crossed over to the
    // next day, so calculate accordingly (we assume that no journey will have a
    // duration greater than a day)
    return arriveTime < departTime ?
        static_cast< float >(arriveTime - 2400 + departTime) / 100 :
        static_cast< float >(arriveTime - departTime) / 100;
}

int Journey::setZone() {
    // If the zones of each station match, then no calculation needed, just set
    // zone to that of either station (we use start station here). If they don't
    // match then we know we are crossing two zones, so set accordingly
    return start.getZone() == end.getZone() ? start.getZone() : 2;
}