#ifndef __USERCOMMANDFACTORY_H__
#define __USERCOMMANDFACTORY_H__

#include <map>
#include <string>

#include "myTicSystem.h"
#include "userCommand.h"
#include "addNewUserCommand.h"
#include "buyJourneyCommand.h"
#include "chargeCardCommand.h"
#include "printUserReportsCommand.h"
#include "saveQuitCommand.h"
#include "showCreditCommand.h"
#include "showStationStatsCommand.h"
#include "updatePricingCommand.h"
#include "nullCommand.h"

using std::map;

class UserCommandFactory {
public:
    UserCommandFactory(MyTicSystem& system);
    ~UserCommandFactory();

    // This function acts as the "factory" in the factory pattern
    UserCommand* make(string command);
private:
    MyTicSystem& system;
    map<string, UserCommand*> commands;
};

#endif
