#ifndef __TRAVELPASSFACTORY_H__
#define __TRAVELPASSFACTORY_H__

#include <sstream>
#include <vector>

#include "travelPass.h"
#include "allDayZone1.h"
#include "allDayZone1And2.h"
#include "twoHoursZone1.h"
#include "twoHoursZone1And2.h"
#include "journey.h"

using std::stringstream;
using std::vector;

class TravelPassFactory {
public:
    class InvalidTravelPassException {
    public:
        string message;
        InvalidTravelPassException(string message) : message(message) {}
    };

    TravelPassFactory();
    ~TravelPassFactory();

    static TravelPass* make(stringstream& input);
private:
    static vector<TravelPass*> instances;
};

#endif