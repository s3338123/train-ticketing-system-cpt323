#ifndef __PRINTPURCHASEREPORT_H__
#define __PRINTPURCHASEREPORT_H__

#include <map>
#include <vector>

#include "function.h"
#include "travelPass.h"

using std::map;
using std::vector;

class PrintPurchaseReport : public Function < vector<TravelPass*>&, void > {
public:
    PrintPurchaseReport();
    ~PrintPurchaseReport();

    virtual void operator() (vector<TravelPass*>& purchases);
    virtual void operator() (vector<Journey*>& journeys);
private:
    string purchaseDayToString(Journey::Day day);
};

#endif