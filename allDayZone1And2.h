//
// allDayZone1And2.h
//
// derived class
//

#ifndef __ALLDAYZONE1AND2_H__
#define __ALLDAYZONE1AND2_H__

#include "travelPass.h"

/*
Derived TravelPass class. Contains class specific data and functions
for retrieving input and output specific to the derived type
*/
class AllDayZone1And2 : public TravelPass {
public:
    static const string PASS_LENGTH;
    static const string PASS_ZONES;
    static const float PASS_COST;

    AllDayZone1And2();
    AllDayZone1And2(string theLength, string theZones, float theCost);
    ~AllDayZone1And2();

    virtual TravelPass* clone() const;
    virtual int getExpiry() const;
    virtual bool isBestMatch(Journey& journey);
    virtual bool isTravelPass(const TravelPass& travelpass);
    virtual bool isValid(Journey& journey);
    virtual void input(); // Data input for a AllDayZone1And2 object
    virtual void print(); // Data output for a AllDayZone1And2 object
};

#endif
