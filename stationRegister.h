#ifndef __STATIONREGISTER_H__
#define __STATIONREGISTER_H__

#include <map>
#include <vector>

#include "station.h"

using std::map;
using std::vector;

/*
A static class intended to track station stats
*/
class StationRegister {
public:
    StationRegister();
    ~StationRegister();

    static void start(const Station& station);
    static void end(const Station& station);

    static int getStarted(const Station& station);
    static int getEnded(const Station& station);
private:
    static map<string, int> started;
    static map<string, int> ended;
};

#endif