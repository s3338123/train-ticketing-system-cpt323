#ifndef __CANUPGRADE_H__
#define __CANUPGRADE_H__

#include "function.h"
#include "myTic.h"

class CanUpgrade : public Function < set<TravelPass*>&, bool > {
public:
    CanUpgrade();
    CanUpgrade(Journey& journey, MyTic *myTic);
    ~CanUpgrade();

    virtual bool operator() (set<TravelPass*>& priceList);
private:
    Journey journey;
    MyTic *myTic;
};

#endif