#ifndef __FULLMYTIC_H__
#define __FULLMYTIC_H__

#include "myTic.h"

class FullMyTic : public MyTic {
public:
    FullMyTic();
    FullMyTic(User* user);
    FullMyTic(float theCredit, User* user);
    ~FullMyTic();

    virtual float getCostOf(TravelPass& travelPass) const;
};

#endif