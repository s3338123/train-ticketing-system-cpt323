#include <iostream>
#include "myTic.h"
#include "menu.h"
#include "menuItem.h"
#include "menuNavigator.h"

using namespace std;

MyTic::MyTic() : credit(0.0f) {}

MyTic::MyTic(User* user) : credit(0.0f), user(user) {}

MyTic::MyTic(float theCredit, User* user) : credit(theCredit), user(user) {}

MyTic::~MyTic() {
    for (auto& purchase : purchases) {
        if (purchase) {
            delete purchase;
            purchase = NULL;
        }
    }
    purchases.clear();
}

void MyTic::input() {
    int amount;
    // Before asking for any amount, make sure the minimum possible
    // doesn't exceed the maximum possible
    if (MAX_CREDIT - credit < MIN_CREDIT_PURCHASE) {
        cout << endl << "Please consider buying some travel passes first" 
            << endl;
        return;
    }
    do {
        cout << endl << "Amount to add: $"; cin >> amount;
        if (!cin) {
            cin.clear();
            cin.ignore();
            amount = 0;
        }
        clearInputBuffer();
    } while (addCredit(amount) == 0);
}

void MyTic::print() {
    cout << "Credit remaining for " << user->getId() << ": $" << credit 
        << endl;
}

void MyTic::buyPass(TravelPass& travelPass, Journey& journey, bool printOut) {
    if (isValidPurchase(travelPass)) {
        credit -= travelPass.getCost();
        travelPass.addJourney(journey);
        purchases.push_back(&travelPass);
        if (printOut) {
            cout << endl << travelPass << " for " << user->getId() << " for $"
                << travelPass.getCost() << endl;
            cout << "Valid until " << travelPass.getExpiry() << endl;
            print();
        }
        // Seeing as we now have a successful journey, increment station stats
        StationRegister::start(journey.getDepartStation());
        StationRegister::end(journey.getArriveStation());
    }
}

void MyTic::buyJourney(TravelPass& travelPass, Journey& journey) {
    cout << "\nCurrent " << travelPass.getLength() << " travel pass still valid" 
        << endl;
    travelPass.addJourney(journey);
    StationRegister::start(journey.getDepartStation());
    StationRegister::end(journey.getArriveStation());
}

// Upgrades the last purchased travel pass to provide better value for users.
void MyTic::buyUpgrade(TravelPass& upgradePass, Journey& journey) {
    string lastPassLength = getLastPurchase()->getLength();
    float lastCost = getLastPurchase()->getCost();
    // refund the previous pass purchase price
    refundPurchase(lastCost);
    // transfer over the journeys
    upgradePass.swapJourneys(purchases.back()->getJourneys());
    // remove the previously purchased pass
    delete purchases.back();
    purchases.back() = NULL;
    purchases.pop_back();
    // then charge them for the upgraded pass
    buyPass(upgradePass, journey, false);
    // calculate the difference in price to display to the user
    float difference = upgradePass.getCost() - lastCost;

    cout << endl << lastPassLength << " travel pass upgraded to " 
        << upgradePass.getLength() << " pass for " << user->getId() << " for $"
        << difference << endl;
    print();
}

// Calculates the running cost of the current journey day, and indicates if it 
// would be
// cheaper to upgrade the existing travel pass
bool MyTic::canUpgrade(Journey& journey, TravelPass& currentPass, 
    TravelPass& newPass, TravelPass& upgrade) {
        float newPassCost = getCostOf(newPass);
        float upgradeCost = getCostOf(upgrade);
        float dailyCost = getCostOf(journey.getDay());

    if (currentPass.getDayOfPurchase() != journey.getDay()) {
        return false;
    }

    // check if all of current day's passes would cost more than the upgrade 
    // cost
    return dailyCost + newPassCost > upgradeCost;
}

bool MyTic::isValidPurchase(const TravelPass& travelPass) {
    if (&travelPass) {
        if (this->getCredit() >= travelPass.getCost() 
            && purchases.size() < MAX_TRAVELPASSES) {
            return true;
        }
        else if (this->getCredit() < travelPass.getCost()) {
            throw InsufficientCreditException
                ("Sorry, you don't have enough credit for that selection");
        }
        else if (purchases.size() >= MAX_TRAVELPASSES) {
            throw MaxTravelPassesException
                ("Unable to store any more travel pass purchases");
        }
    }

    return false;
}

void MyTic::printPurchases() {
    PrintPurchaseReport()(purchases);
}

float MyTic::getCostOf(Journey::Day day) const {
    // Traverse backward through purchases until the day no longer matches
    float cost = 0.0f;
    for (unsigned i = purchases.size(); i-- > 0;) {
        cost += purchases.at(i)->getCost();
        // While iterating backwards, if the previous travel pass is the same 
        // day as the function argument, then we add it to the total cost. If we
        // come across a scenario where it appears a pass has travelled back in
        // time, we will assume that there was in fact just a weeks gap between
        // the two passes (i.e. the pass was purchased on the same day a week 
        // apart).
        vector<Journey*> journeys = purchases.at(i)->getJourneys();
        int departTime = journeys.at(journeys.size()-1)->getDepartTime();
        int previousDepartTime = departTime;
        for (unsigned j = journeys.size(); j-- > 0;) {
            if (departTime > previousDepartTime) return cost;
        }
    }
    
    return cost;
}

float MyTic::getCredit() const {
    return this->credit;
}

vector<TravelPass*> MyTic::getPurchases() const {
    return purchases;
}

TravelPass* MyTic::getLastPurchase() const {
    if (purchases.empty()) return NULL;

    try {
        return purchases.back();
    }
    catch (std::out_of_range) {
        return NULL;
    }
}

void MyTic::refundPurchase(float refund) {
    credit += refund;
}

int MyTic::addCredit(int amount) {
    int amountCredited = 0;
    if (amount % MIN_CREDIT_PURCHASE != 0) {
        throw CreditMultipleException("You can only add multiples of $" 
            + to_string(MIN_CREDIT_PURCHASE));
    }
    else if (credit + amount > MAX_CREDIT) {
        throw MaxCreditException("Max amount of credit allowed is $" 
            + to_string(MAX_CREDIT));
    }
    else {
        credit += amount;
        amountCredited = amount;
    }

    return amountCredited;
}
