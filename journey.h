#ifndef __JOURNEY_H__
#define __JOURNEY_H__

#include <iostream>
#include <string>
#include <algorithm>

#include "station.h"

using std::istream;
using std::string;

class Journey {
public:
    enum Day { sun, mon, tue, wed, thu, fri, sat };

    // Declare a base exception class to make it simpler to catch exceptions
    // where a catch all is convenient
    class JourneyException {
    public:
        string message;
        JourneyException(string message) : message(message) {}
    };

    class InvalidDayException : public JourneyException {
    public:
        InvalidDayException(string message) : JourneyException(message) {}
    };

    class InvalidStationException : public JourneyException {
    public:
        InvalidStationException(string message) : JourneyException(message) {}
    };

    class InvalidTimeException : public JourneyException {
    public:
        InvalidTimeException(string message) : JourneyException(message) {}
    };

    Journey();
    Journey(Station& start, Station& end, string day, int departTime, 
        int arriveTime);
    ~Journey();

    // Define accessors
    Day getDay() const;
    float getDuration() const;
    int getZone() const;
    Station getDepartStation() const;
    int getDepartTime() const;
    Station getArriveStation() const;
    int getArriveTime() const;
private:
    Station start;
    Station end;
    Day day;
    int departTime;
    int arriveTime;
    float duration;
    int zone;

    // A journey's details should not be altered, so we declare mutators as 
    // private
    Day setDay(string day);
    float setDuration();
    int setZone();
};

#endif