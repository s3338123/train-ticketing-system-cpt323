#ifndef __USER_H__
#define __USER_H__

#include <iostream>
#include <string>

#include "myTic.h"

using std::istream;
using std::ostream;
using std::string;

class User {
public:
    User();
    User(MyTic* myTic);
    User(string id, string name, string email, MyTic* myTic);
    virtual ~User();

    friend istream& operator >>(istream& input, User& user);
    friend ostream& operator <<(ostream& output, const User& user);

    string getId() const;
    string getType() const;
    string getName() const;
    string getEmail() const;
    virtual MyTic* getMyTic() const;

    void setId(const string id);
    void setType(const string type);
    void setName(const string name);
    void setEmail(const string email);
    virtual void setMyTic(MyTic* myTic);
private:
    string id;
    string type;
    string name;
    string email;
    MyTic* ticket;
};

#endif