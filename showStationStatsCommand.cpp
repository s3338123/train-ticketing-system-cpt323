#include "showStationStatsCommand.h"

ShowStationStatsCommand::ShowStationStatsCommand(MyTicSystem &system) 
    : system(system) {}

ShowStationStatsCommand::~ShowStationStatsCommand() {}

int ShowStationStatsCommand::execute() {
    system.showStationStats();
    return 1;
}