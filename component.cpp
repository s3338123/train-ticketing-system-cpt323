#include "component.h"

Component::Component(char menuChoice, string menuTitle) 
	: menuChoice(menuChoice), menuTitle(menuTitle) {}

Component::~Component() {}

char Component::getChoice() const {
	return this->menuChoice;
}

string Component::getTitle() const {
	return this->menuTitle;
}
