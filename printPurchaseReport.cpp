#include "printPurchaseReport.h"

PrintPurchaseReport::PrintPurchaseReport() {}

PrintPurchaseReport::~PrintPurchaseReport() {}

void PrintPurchaseReport::operator()(vector<TravelPass*>& purchases) {
    if (purchases.empty()) {
        cout << "No purchases have been made." << endl;
        return;
    }

    int i = 1;
    for (auto& purchase : purchases) {
        cout << i << ". ";
        purchase->print();
        cout << " on " 
            << purchaseDayToString(purchase->getDayOfPurchase()) << " at " 
            << purchase->getTimeOfPurchase() << " for $" 
            << purchase->getCost() << endl;
        PrintPurchaseReport()(purchase->getJourneys());
        i++;
    }
}

void PrintPurchaseReport::operator()(vector<Journey*>& journeys) {
    if (journeys.empty()) {
        cout << "\tNo journeys have been made." << endl;
        return;
    }

    cout << "Journeys on this pass:\n";
    for (auto& journey : journeys) {
        cout << "\t- " << journey->getDepartStation().getName()
            << " to " << journey->getArriveStation().getName()
            << " at " << journey->getDepartTime() << endl;
    }
}

string PrintPurchaseReport::purchaseDayToString(Journey::Day day) {
    switch (day) {
        case Journey::Day::sun: return "Sunday";
        case Journey::Day::mon: return "Monday";
        case Journey::Day::tue: return "Tuesday";
        case Journey::Day::wed: return "Wednesday";
        case Journey::Day::thu: return "Thursday";
        case Journey::Day::fri: return "Friday";
        case Journey::Day::sat: return "Saturday";
        default: return NULL;
    }
}