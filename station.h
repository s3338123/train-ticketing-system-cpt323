#ifndef __STATION_H__
#define __STATION_H__

#include <iostream>
#include <string>

using std::istream;
using std::ostream;
using std::string;

class Station {
public:
    // Declare a base exception class to make it simpler to catch exceptions 
    // where a catch all is convenient
    class StationException {
    public:
        string message;
        StationException(string message) : message(message) {}
    };

    class InvalidStationNameException : public StationException {
    public:
        InvalidStationNameException(string message) 
            : StationException(message) {}
    };

    class InvalidStationZoneException : public StationException {
    public:
        InvalidStationZoneException(string message) 
            : StationException(message) {}
    };

    Station();
    Station(string name, int zone);
    ~Station();

    friend istream& operator >> (istream& input, Station& station);
    friend ostream& operator << (ostream& output, const Station& station);

    string getName() const;
    int getZone() const;

    void setName(const string name);
    void setZone(const int zone);
private:
    string name;
    int zone;
};

#endif