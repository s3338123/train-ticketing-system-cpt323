#ifndef __VISITOR_H__
#define __VISITOR_H__

class Menu;
class MenuItem;

/*
Base class for our Visitor pattern. Allows type based decisions to be
made at runtime without resorting to casting.
*/
class Visitor {
public:
	Visitor();
	virtual ~Visitor();

	virtual void visit(Menu& item) const = 0;
	virtual void visit(MenuItem& item) const = 0;
};

#endif
