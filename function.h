#ifndef __FUNCTION_H__
#define __FUNCTION_H__

/*
Template class which also acts as a function object through the overloaded 
() operator.
*/
template <class IN, class OUT>
class Function {
public:
    Function() {}
    virtual ~Function() {}

    virtual OUT operator() (IN in) = 0;
};

#endif