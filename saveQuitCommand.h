#ifndef __SAVEQUITCOMMAND_H__
#define __SAVEQUITCOMMAND_H__

#include "userCommand.h"
#include "myTicSystem.h"

class SaveQuitCommand : public UserCommand {
public:
    SaveQuitCommand(MyTicSystem& system);
    ~SaveQuitCommand();

    virtual int execute();
private:
    MyTicSystem system;
};

#endif