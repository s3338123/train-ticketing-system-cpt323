#ifndef __BUYJOURNEYCOMMAND_H__
#define __BUYJOURNEYCOMMAND_H__

#include "userCommand.h"
#include "myTicSystem.h"

class BuyJourneyCommand : public UserCommand {
public:
    BuyJourneyCommand(MyTicSystem& system);
    ~BuyJourneyCommand();

    virtual int execute();
private:
    MyTicSystem system;
};

#endif