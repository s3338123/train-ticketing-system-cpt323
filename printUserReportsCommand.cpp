#include "printUserReportsCommand.h"

PrintUserReportsCommand::PrintUserReportsCommand(MyTicSystem &system) 
    : system(system) {}

PrintUserReportsCommand::~PrintUserReportsCommand() {}

int PrintUserReportsCommand::execute() {
    system.printUserReports();
    return 1;
}
