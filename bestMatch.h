#ifndef __BESTMATCH_H__
#define __BESTMATCH_H__

#include <set>

#include "function.h"
#include "journey.h"
#include "travelPass.h"

using std::set;

class BestMatch : public Function < set<TravelPass*>&, TravelPass* > {
public:
    BestMatch();
    BestMatch(Journey &journey);
    ~BestMatch();

    virtual TravelPass* operator() (set<TravelPass*>& priceList);
private:
    Journey journey;
};

#endif