#include "addNewUserCommand.h"

AddNewUserCommand::AddNewUserCommand(MyTicSystem& system) : system(system) {}

AddNewUserCommand::~AddNewUserCommand() {}

int AddNewUserCommand::execute() {
    system.addNewUser();
    return 1;
}