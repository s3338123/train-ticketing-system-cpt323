#include "user.h"

User::User() {}

User::User(MyTic* ticket) : ticket(ticket) {}

User::User(string id, string name, string email, MyTic* ticket) 
    : id(id), name(name), email(email), ticket(ticket) {}

User::~User() {
    if (ticket) {
        delete ticket;
        ticket = NULL;
    }
}

istream& operator >>(istream& input, User& user) {
    string id, type, name, email;
    int initialCredit;

    getline(input, id, ':');
    getline(input, type, ':');
    getline(input, name, ':');
    getline(input, email, ':') >> initialCredit;

    user.setId(id);
    user.setType(type);
    user.setName(name);
    user.setEmail(email);
    try {
        user.getMyTic()->addCredit(initialCredit);
    }
    catch (MyTic::CreditMultipleException e) {
        std::cerr << "Attempt to add incorrect credit amount for user " << id 
            << endl;
        std::cerr << e.message << endl;
        std::cerr << "Setting users credit to $0" << endl << endl;
        user.getMyTic()->addCredit(0);
    }
    catch (MyTic::MaxCreditException e) {
        std::cerr << "Attempt to add more than maximum allowed credit for user "
            << id << endl;
        std::cerr << e.message << endl;
        std::cerr << "Setting users credit to maximum only" << endl << endl;
        user.getMyTic()->addCredit(MyTic::MAX_CREDIT);
    }

    return input;
}

ostream& operator <<(ostream& output, const User& user) {
    output << user.getId() << ':' << user.getType() << ':' << user.getName() 
        << ':' << user.getEmail() << ':' << user.getMyTic()->getCredit();
    return output;
}

string User::getId() const {
    return id;
}

string User::getType() const {
    return type;
}

string User::getName() const {
    return name;
}

string User::getEmail() const {
    return email;
}

MyTic* User::getMyTic() const {
    return ticket;
}

void User::setId(const string id) {
    this->id = id;
}

void User::setType(const string type) {
    this->type = type;
}

void User::setName(const string name) {
    this->name = name;
}

void User::setEmail(const string email) {
    this->email = email;
}

void User::setMyTic(MyTic* myTic) {
    ticket = myTic;
}