#include "menu.h"
#include "menuItem.h"
#include "runVisitor.h"

RunVisitor::RunVisitor(MenuNavigator& navigator) : navigator(navigator) {}

RunVisitor::~RunVisitor() {}

void RunVisitor::visit(Menu& item) const {
	navigator.load(item);
}

void RunVisitor::visit(MenuItem& item) const {
	int result = item.run();
	navigator.exit(result);
}
