#include "canUpgrade.h"

CanUpgrade::CanUpgrade() {}

CanUpgrade::CanUpgrade(Journey& journey, MyTic *myTic) 
    : journey(journey), myTic(myTic) {}

CanUpgrade::~CanUpgrade() {}

bool CanUpgrade::operator()(set<TravelPass*>& priceList) {
    float newPassCost = 0.0f;
    float upgradeCost = 0.0f;
    float dailyCost = myTic->getCostOf(journey.getDay());

    set<TravelPass*>::iterator it;
    for (it = priceList.begin(); it != priceList.end(); ++it) {
        // get the theoretical cost of a new pass for the user
        if ((*it)->isBestMatch(journey)) {
            newPassCost = myTic->getCostOf(*(*it));
        }
        // get the upgrade cost while we are looping
        if (journey.getZone() == 2 && (*it)->isTravelPass(AllDayZone1And2())) {
            upgradeCost = myTic->getCostOf(*(*it));
        }
        else if (journey.getZone() == 1 && (*it)->isTravelPass(AllDayZone1())) {
            upgradeCost = myTic->getCostOf(*(*it));
        }
    }

    // Issues with c++11 support on uni servers
    //for (auto& pass : priceList) {
    //    // get the theoretical cost of a new pass for the user
    //    if (pass->isBestMatch(journey)) {
    //        newPassCost = myTic->getCostOf(*pass);
    //    }
    //    // get the upgrade cost while we are looping
    //    if (journey.getZone() == 2 && pass->isTravelPass(AllDayZone1And2())) 
    //    {
    //        upgradeCost = myTic->getCostOf(*pass);
    //    }
    //    else if (journey.getZone() == 1 && pass->isTravelPass(AllDayZone1())){
    //        upgradeCost = myTic->getCostOf(*pass);
    //    }
    //}

    // check if all of current day's passes would cost more than the daily pass
    // that matches the journeys zone
    bool result = dailyCost + newPassCost > upgradeCost;

    return result;
}