// testTravelPass.cpp for CPT 323 Assignment 1 SP3 2014
//
//
// CPT323 2014 assignment 2
/////////////////////////////////////////////////////

#include "testTravelPass.h"

void parseStations(string stationsFile, map<string, Station*>& stations);
void splitTicketsFile(string ticketsFile, stringstream& users, 
    stringstream& tickets);
void parseUsers(stringstream& userStream, map<string, User*>& users);
void parseTickets(stringstream& ticketStream, set<TravelPass*>& tickets);

int main(int argc, char *argv[]) {
    
    // Check command line input
    if (argc != 4) {
        cerr << "usage: " << argv[0] 
            << " <stations file> <myTic file> <output file>\n";
        exit(1);
    }

    map<string, User*> users;
    set<TravelPass*> passes;
    map<string, Station*> stations;
    stringstream userStream;
    stringstream ticketStream;

    parseStations(argv[1], stations);
    splitTicketsFile(argv[2], userStream, ticketStream);
    parseUsers(userStream, users);
    parseTickets(ticketStream, passes);

    ofstream outputStream(argv[3]);
    if (outputStream.fail()) {
        cerr << "Failed to open " << argv[3] << "file.\n";
        exit(1);
    }

    MyTicSystem myTicSystem(users, passes, stations);
    UserCommandFactory command(myTicSystem);
    MenuNavigator navigator;

    // Create menu objects
    Menu menu("Main Menu");
    MenuItem buyJourney
        ('1', "Buy a journey for a user", *command.make("buyJourney"));
    MenuItem rechargeCard
        ('2', "Recharge a MyTic card for a User", *command.make("chargeCard"));
    MenuItem showUserCredit
        ('3', "Show remaining credit for a User", 
            *command.make("showUserCredit"));
    MenuItem printUserReports
        ('4', "Print User Reports", *command.make("printUserReports"));
    MenuItem updatePricing
        ('5', "Update pricing", *command.make("updatePricing"));
    MenuItem showStationStats
        ('6', "Show Station statistics", *command.make("showStationStats"));
    MenuItem addNewUser
        ('7', "Add a new User", *command.make("addNewUser"));
    MenuItem quitCommand
        ('0', "Save/Quit", *command.make("saveQuit"));

    menu.add(buyJourney);
    menu.add(rechargeCard);
    menu.add(showUserCredit);
    menu.add(printUserReports);
    menu.add(updatePricing);
    menu.add(showStationStats);
    menu.add(addNewUser);
    menu.add(quitCommand);

    navigator.load(menu);

    // clean up
    myTicSystem.saveTo(outputStream);
    outputStream.close();

    for (auto it : users) delete it.second;
    for (auto pass : passes) delete pass;
    for (auto it : stations) delete it.second;

    users.clear();
    passes.clear();
    stations.clear();
    
    return 0;
}

void parseStations(string stationsFile, map<string, Station*>& stations) {
    // First command line argument is stations file, so let's read that in.
    // Going to want to do some error checking here
    ifstream stationStream(stationsFile);
    if (stationStream.fail()) {
        cerr << "Failed to open " << stationsFile << " file.\n";
        exit(1);
    }

    string next;
    stringstream ss;

    while (stationStream >> next) {
        ss.clear();
        ss.str(next);
        Station *station = NULL;
        try {
            station = new Station;
            ss >> *station;
            stations[station->getName()] = station;
        }
        catch (const Station::StationException e) {
            cerr << e.message << endl;
            if (station) {
                delete station;
                station = NULL;
            }
        }
    }

    stationStream.close();
}

void splitTicketsFile(string ticketsFile, stringstream& users, 
    stringstream& tickets) {
    // Second command line argument in myTic file, so let's read that in
    ifstream myTicStream(ticketsFile);
    if (myTicStream.fail()) {
        cerr << "Failed to open " << ticketsFile << " file.\n";
        exit(1);
    }

    string next;
    bool isUser;

    while (getline(myTicStream, next)) {
        // Performing below check on each iteration means there doesn't have to 
        // be any guarantee that the ticket stream is neatly split into either
        // users or prices, it can alternate betwen the two and we will pick it 
        // up as long as it has been  correctly marked with the relevant heading
        if (next.find("#users") != string::npos) {
            isUser = true;
            continue; // do not process header line
        }
        else if (next.find("#prices") != string::npos) {
            isUser = false;
            continue; // do not process header line
        }
        else if (next.empty()) { // do not process empty strings
            continue;
        }

        // We can now figure out if we have a user or a travel pass
        if (isUser) {
            users << next << endl;
        }
        else {
            tickets << next << endl;
        }
    }

    myTicStream.close();
}

void parseUsers(stringstream& userStream, map<string, User*>& users) {
    string next;
    stringstream ss;

    while (getline(userStream, next)) {
        ss.clear();
        ss.str(next);
        User* user = NULL;
        try {
            user = UserFactory::make(ss);
            users[user->getId()] = user;
        }
        catch (const UserFactory::DuplicateUserException& e) {
            cerr << e.message << endl;
            cerr << "This user will not be added" << endl << endl;
            if (user) {
                delete user;
                user = NULL;
            }
        }
        catch (const UserFactory::InvalidUserTypeException& e) {
            cerr << e.message << endl;
            cerr << "This user will not be added" << endl << endl;
            if (user) {
                delete user;
                user = NULL;
            }
        }
    }
}

void parseTickets(stringstream& ticketStream, set<TravelPass*>& tickets) {
    string next;
    stringstream ss;

    while (getline(ticketStream, next)) {
        ss.clear();
        ss.str(next);
        try {
            tickets.insert(TravelPassFactory::make(ss));
        }
        catch (TravelPassFactory::InvalidTravelPassException e) {
            cerr << e.message << endl;
            cerr << "This travel pass will not be added" << endl << endl;
        }
    }
}
