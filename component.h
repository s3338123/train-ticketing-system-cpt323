#ifndef __COMPONENT_H__
#define __COMPONENT_H__

#include <string>

using std::string;

class Visitor;

/*
Base class for menu's and menu items.
This class represents the Component in the Composite pattern
*/
class Component {
public:
    Component(char menuChoice, string menuTitle);
    virtual ~Component();

    virtual int run() const = 0;
    virtual Component* item(char choice) = 0;
    virtual void accept(const Visitor& v) = 0;

    virtual char getChoice() const;
    virtual string getTitle() const;
protected:
    char menuChoice;
    string menuTitle;
};

#endif
