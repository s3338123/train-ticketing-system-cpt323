#ifndef __SHOWCREDITCOMMAND_H__
#define __SHOWCREDITCOMMAND_H__

#include "userCommand.h"
#include "myTicSystem.h"

class ShowCreditCommand : public UserCommand {
public:
    ShowCreditCommand();
    ShowCreditCommand(MyTicSystem& system);
    ~ShowCreditCommand();

    virtual int execute();
private:
    MyTicSystem system;
};

#endif