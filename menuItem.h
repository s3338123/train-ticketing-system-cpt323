#ifndef __MENUITEM_H__
#define __MENUITEM_H__

#include "component.h"
#include "userCommand.h"

using std::string;

/*
Stores data and functions for operation on menu items.
This class represents the leaf node in the Composite pattern.
*/
class MenuItem : public Component {
public:
    MenuItem(char menuChoice, string menuTitle, UserCommand& command);
    ~MenuItem();

    // executes the command associated with this menu item
    virtual int run() const;
    // returns this current object
    virtual Component* item(char choice);
    // accepts and executes the visit method for the visitor pattern
    virtual void accept(const Visitor& v);
private:
    UserCommand* command;
};

#endif
