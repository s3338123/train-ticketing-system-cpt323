#ifndef __MENUNAVIGATOR_H__
#define __MENUNAVIGATOR_H__

#include <iostream>

#include "component.h"
#include "utility1.h"

using std::cin;
using std::cout;
using std::endl;

/*
Simple utility class that handles navigation of menus.
Provides ability to retrieve menu selections made by users.
*/
class MenuNavigator {
public:
    MenuNavigator();
    ~MenuNavigator();

    void load(Component& menu);
    void exit(int command);

    Component* getSelection() const;
private:
    // You could always turn this into a vector to store a selection history
    Component* lastSelected;
    bool exitMenu;
};

#endif
