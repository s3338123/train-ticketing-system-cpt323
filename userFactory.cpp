#include "userFactory.h"

vector<User*> UserFactory::instances;

UserFactory::UserFactory() {}

UserFactory::~UserFactory() {
    for (auto& user : instances) {
        if (user) {
            delete user;
            user = NULL;
        }
    }
    instances.clear();
}

User* UserFactory::make(stringstream& input) {
    string userId;
    string userType;
    stringstream initialiser;

    // Create a copy of input, because we need to remove some values
    // to determine the user type, but we want the full stream to
    // initialise the users values
    initialiser << input.str();

    getline(input, userId, ':');
    getline(input, userType, ':');

    // Duplicate id's are not allowed
    for (auto existingUser : instances) {
        if (userId == existingUser->getId()) {
            throw UserFactory::DuplicateUserException
                ("User id already exists: " + userId);
        }
    }

    User* user = new User();
    MyTic* ticket = NULL;

    if (userType == "adult") {
        ticket = new FullMyTic(user);
    }
    else if (userType == "junior") {
        ticket = new JuniorMyTic(user);
    }
    else if (userType == "senior") {
        ticket = new SeniorMyTic(user);
    }
    else {
        if (user) {
            delete user;
            user = NULL;
        }
        if (ticket) {
            delete ticket;
            ticket = NULL;
        }
        throw UserFactory::InvalidUserTypeException("Invalid user type: " 
            + userType);
    }

    user->setMyTic(ticket);
    initialiser >> *user;

    // Add users to a container so we can free up memory later
    instances.push_back(user);

    return user;
}