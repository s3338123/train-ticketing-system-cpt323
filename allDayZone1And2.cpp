#include "allDayZone1And2.h"

const string AllDayZone1And2::PASS_LENGTH = "All Day";
const string AllDayZone1And2::PASS_ZONES = "Zone 1 and 2";
const float AllDayZone1And2::PASS_COST = 6.80f;

AllDayZone1And2::AllDayZone1And2() 
    : TravelPass(PASS_LENGTH, PASS_ZONES, PASS_COST) {}

AllDayZone1And2::AllDayZone1And2
(string theLength, string theZones, float theCost) 
    : TravelPass(theLength, theZones, theCost) {}

AllDayZone1And2::~AllDayZone1And2() {}

TravelPass* AllDayZone1And2::clone() const {
    return new AllDayZone1And2(*this);
}

int AllDayZone1And2::getExpiry() const {
    return 2359;
}

bool AllDayZone1And2::isBestMatch(Journey& journey) {
    return (journey.getDuration() > 2 && journey.getZone() == 2);
}

bool AllDayZone1And2::isValid(Journey& journey) {
    return journeys.front()->getDay() == journey.getDay();
}

bool AllDayZone1And2::isTravelPass(const TravelPass& travelPass) {
    return (typeid(*this) == typeid(travelPass));
}

void AllDayZone1And2::input() {
    cin >> *this;
}

void AllDayZone1And2::print() {
    cout << *this;
}
