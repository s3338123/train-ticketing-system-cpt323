#ifndef __PRINTUSERREPORTSCOMMAND_H__
#define __PRINTUSERREPORTSCOMMAND_H__

#include "userCommand.h"
#include "myTicSystem.h"

class PrintUserReportsCommand : public UserCommand {
public:
    PrintUserReportsCommand(MyTicSystem& system);
    ~PrintUserReportsCommand();

    virtual int execute();
private:
    MyTicSystem system;
};

#endif