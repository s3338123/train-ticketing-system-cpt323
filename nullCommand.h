#ifndef __NULLCOMMAND_H__
#define __NULLCOMMAND_H__

#include "userCommand.h"

/*
Encapsulates the function required to charge a travel pass.
This class acts as the Concrete Command in the Command pattern.
*/
class NullCommand : public UserCommand {
public:
    NullCommand();
	~NullCommand();

	virtual int execute();
};

#endif
