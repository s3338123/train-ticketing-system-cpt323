//
// travelPass.h
//
// Parent TravelPass class
//

#ifndef __TRAVELPASS_H__
#define __TRAVELPASS_H__

#include <iostream>
#include <string>
#include <typeinfo>
#include <vector>

#include "journey.h"
#include "utility1.h"

using std::istream;
using std::ostream;
using std::string;
using std::vector;

using std::cin;
using std::cout;
using std::endl;
using std::getline;

/*
Base class for TravelPass family of classes.
*/
class TravelPass {
public:
    class TravelPassException {
    public:
        string message;
        TravelPassException(string message) : message(message) {}
    };

    class TimeTravelException : public TravelPassException {
    public:
        TimeTravelException(string message) : TravelPassException(message) {}
    };

	TravelPass();
	TravelPass(string theLength, string theZones, float theCost);
	virtual ~TravelPass();

    friend istream& operator>>(istream& input, TravelPass& travelPass);
    friend ostream& operator<<(ostream& output, const TravelPass& travelPass);

    bool canBuy(Journey& journey);

    virtual TravelPass* clone() const = 0;
    virtual bool isBestMatch(Journey& journey) = 0;
    virtual bool isTravelPass(const TravelPass& travelPass) = 0;
    virtual bool isValid(Journey& journey) = 0;
    virtual void input() = 0; // Data input for a TravelPass object
    virtual void print() = 0; // Data output for a TravelPass object

    // Define accessors
    float getCost() const;
    Journey::Day getDayOfPurchase() const;
    virtual int getExpiry() const = 0;
    vector<Journey*>& getJourneys();
	string getLength() const; //Note the use of const
    int getTimeOfPurchase() const;
	string getZones() const;

    // Define mutators
    void addJourney(Journey& journey);
    void swapJourneys(vector<Journey*>& copy);
	void setLength(string newLength);
	void setZones(string newZones);
	void setCost(float newCost);
protected:
    string length;
    string zones;
    float cost;
    vector<Journey*> journeys;
};

#endif
