#ifndef __CHARGECARDCOMMAND_H__
#define __CHARGECARDCOMMAND_H__

#include "userCommand.h"
#include "myTicSystem.h"

class ChargeCardCommand : public UserCommand {
public:
    ChargeCardCommand(MyTicSystem& system);
    ~ChargeCardCommand();

    virtual int execute();
private:
    MyTicSystem system;
};

#endif