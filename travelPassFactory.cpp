#include "travelPassFactory.h"

vector<TravelPass*> TravelPassFactory::instances;

TravelPassFactory::TravelPassFactory() {}

TravelPassFactory::~TravelPassFactory() {
    for (auto& instance : instances) {
        if (instance) {
            delete instance;
            instance = NULL;
        }
    }
    instances.clear();
}

TravelPass* TravelPassFactory::make(stringstream& input) {
    string length;
    string zone;
    stringstream initialiser;

    // Create a copy of input, because we need to remove some values
    // to determine the travel pass type, but we want the full stream to
    // initialise the travel pass values
    initialiser << input.str();

    getline(input, length, ':');
    getline(input, zone, ':');

    string selection = length + " " + zone;
    TravelPass* travelPass = NULL;

    if (selection == "2Hour Zone1") travelPass = new TwoHoursZone1;
    else if (selection == "2Hour Zone12") travelPass = new TwoHoursZone1And2;
    else if (selection == "AllDay Zone1") travelPass = new AllDayZone1;
    else if (selection == "AllDay Zone12") travelPass = new AllDayZone1And2;
    else throw TravelPassFactory::
        InvalidTravelPassException("Invalid travel pass: " + selection);
    
    initialiser >> *travelPass;

    // Add passes to a container so we can free up memory later
    instances.push_back(travelPass);

    return travelPass;
}