#include "bestMatch.h"

BestMatch::BestMatch() {}

BestMatch::BestMatch(Journey &journey) : journey(journey) {}

BestMatch::~BestMatch() {}

TravelPass* BestMatch::operator()(set<TravelPass*>& priceList) {
    set<TravelPass*>::iterator it;
    for (it = priceList.begin(); it != priceList.end(); ++it) {
        if ((*it)->isBestMatch(journey)) {
            return (*it)->clone();
        }
    }

    // Struggling to get c++11 features working on uni servers
    //for (auto& pass : priceList) {
    //    if (pass->isBestMatch(journey)) {
    //        return pass->clone();
    //    }
    //}

    return NULL;
}