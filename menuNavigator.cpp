#include "menuNavigator.h"
#include "runVisitor.h"

MenuNavigator::MenuNavigator() : exitMenu(false) {
    cout.setf(std::ios::fixed);
    cout.setf(std::ios::showpoint);
    cout.precision(2);
}

MenuNavigator::~MenuNavigator() {}

void MenuNavigator::load(Component& menu) {
    RunVisitor runVisitor(*this);
    char choice;
    do {
        cout << endl;
        menu.run();
        cout << "Please make a selection: "; cin >> choice;
        clearInputBuffer();
        Component& selection = *menu.item(tolower(choice));
        if (&selection) {
            selection.accept(runVisitor);
            this->lastSelected = &selection;
        }
        else {
            cout << "Invalid choice" << endl;
        }
    }
    while (!this->exitMenu);
    // If load() is being called recursively, then make sure exitMenu 
    // is reset to avoid the do/while loop ending prematurely
    this->exitMenu = false;
}

Component* MenuNavigator::getSelection() const {
    return this->lastSelected;
}

void MenuNavigator::exit(int command) {
    this->exitMenu = command == 0 ? true : false;
}
