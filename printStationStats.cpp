#include "printStationStats.h"

PrintStationStats::PrintStationStats() {}

PrintStationStats::~PrintStationStats() {}

Station* PrintStationStats::operator() (std::pair<string, Station*> pair) {
    Station* station = pair.second;
    cout << pair.first << ": "
        << StationRegister::getStarted(*station) << " journeys started here, "
        << StationRegister::getEnded(*station) << " journeys ended here\n";

    return pair.second;
}