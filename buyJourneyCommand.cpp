#include "buyJourneyCommand.h"

BuyJourneyCommand::BuyJourneyCommand(MyTicSystem &system) : system(system) {}

BuyJourneyCommand::~BuyJourneyCommand() {}

int BuyJourneyCommand::execute() {
    system.buyJourney();
    return 1;
}