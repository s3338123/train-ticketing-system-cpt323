#include "updatePricingCommand.h"

UpdatePricingCommand::UpdatePricingCommand(MyTicSystem &system) 
    : system(system) {}

UpdatePricingCommand::~UpdatePricingCommand() {}

int UpdatePricingCommand::execute() {
    system.updatePricing();
    return 1;
}