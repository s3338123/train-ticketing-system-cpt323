#ifndef __USERFACTORY_H__
#define __USERFACTORY_H__

#include <iostream>
#include <vector>

#include "user.h"
#include "fullMyTic.h"
#include "juniorMyTic.h"
#include "seniorMyTic.h"

using std::istream;
using std::vector;

class UserFactory {
public:
    class DuplicateUserException {
    public:
        string message;
        DuplicateUserException(string message) : message(message) {}
    };
    class InvalidUserTypeException {
    public:
        string message;
        InvalidUserTypeException(string message) : message(message) {}
    };

    UserFactory();
    ~UserFactory();

    static User* make(stringstream& input);
private:
    static vector<User*> instances;
    static vector<MyTic*> ticketInstances;
};

#endif