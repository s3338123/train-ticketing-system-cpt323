#ifndef __ADDNEWUSERCOMMAND_H__
#define __ADDNEWUSERCOMMAND_H__

#include "userCommand.h"
#include "myTicSystem.h"

class AddNewUserCommand : public UserCommand {
public:
    AddNewUserCommand();
    AddNewUserCommand(MyTicSystem& system);
    ~AddNewUserCommand();

    virtual int execute();
private:
    MyTicSystem system;
};

#endif