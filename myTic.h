//
//myTic.h
//
// MyTic class
//

#ifndef __MYTIC_H__
#define __MYTIC_H__

#include <vector>
#include <set>

#include "function.h"
#include "utility1.h"
#include "travelPass.h"
#include "travelPassFactory.h"
#include "allDayZone1.h"
#include "allDayZone1And2.h"
#include "twoHoursZone1.h"
#include "twoHoursZone1And2.h"
#include "journey.h"
#include "bestMatch.h"
#include "bestUpgrade.h"
#include "printPurchaseReport.h"
#include "stationRegister.h"

class User;

using std::vector;

/*
Provides basic functions for adding and viewing credit and purchasing travel
passes.
*/
class MyTic {
public:
    static const unsigned int MIN_CREDIT_PURCHASE = 5;
    static const unsigned int MAX_CREDIT = 100;
    static const unsigned int MAX_TRAVELPASSES = 100;

    // Declare a base exception class to make it simpler to catch exceptions
    // where a catch all is convenient
    class MyTicException {
    public:
        string message;
        MyTicException(string message) : message(message) {}
    };

    class CreditMultipleException : public MyTicException {
    public:
        CreditMultipleException(string message) : MyTicException(message) {}
    };

    class InsufficientCreditException : public MyTicException {
    public:
        InsufficientCreditException(string message) : MyTicException(message) {}
    };

    class MaxCreditException : public MyTicException {
    public:
        MaxCreditException(string message) : MyTicException(message) {}
    };

    class MaxTravelPassesException : public MyTicException {
    public:
        MaxTravelPassesException(string message) : MyTicException(message) {}
    };

    MyTic();
    MyTic(User* user);
    MyTic(float theCredit, User* user);
    virtual ~MyTic();

    void input(); //Data input for a MyTic object
    void print(); //Data output for a MyTic object
    
    virtual void buyPass(TravelPass& travelPass, Journey& journey, 
        bool printOut = true);
    virtual void buyJourney(TravelPass& travelPass, Journey& journey);
    void buyUpgrade(TravelPass& travelPass, Journey& journey);
    bool canUpgrade(Journey& journey, TravelPass& currentPass, 
        TravelPass& newPass, TravelPass& upgradePass);
    void printPurchases(); //Data output for all purchased travel passes

    // Calculate the total cost of a users travel pass
    virtual float getCostOf(TravelPass& travelPass) const = 0;
    // Calculate the total cost of all travel passes for a given day
    float getCostOf(Journey::Day day) const;
    // Retrieve the current credit on a users myTic
    float getCredit() const;
    // Retrieve all purchases
    vector<TravelPass*> getPurchases() const;
    // Retrieve the last travel purchased on this myTic
    TravelPass* getLastPurchase() const;

    int addCredit(int amount);
protected:
    vector <TravelPass*> purchases;
private:
    float credit;
    bool isValidPurchase(const TravelPass& travelPass);
    void refundPurchase(float refund);
    User* user;
};

#endif
