#ifndef __JUNIORMYTIC_H__
#define __JUNIORMYTIC_H__

#include "myTic.h"

class JuniorMyTic : public MyTic {
public:
    static const float DISCOUNT_RATE;

    JuniorMyTic();
    JuniorMyTic(User* user);
    JuniorMyTic(float theCredit, User* user);
    ~JuniorMyTic();

    virtual void buyPass(TravelPass& travelPass, Journey& journey, 
        bool printOut = true);
    virtual float getCostOf(TravelPass& travelPass) const;
};

#endif