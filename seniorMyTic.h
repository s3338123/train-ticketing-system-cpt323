#ifndef __SENIORMYTIC_H__
#define __SENIORMYTIC_H__

#include "myTic.h"

class SeniorMyTic : public MyTic {
public:
    static const float DISCOUNT_RATE;

    SeniorMyTic();
    SeniorMyTic(User* user);
    SeniorMyTic(float theCredit, User* user);
    ~SeniorMyTic();

    virtual void buyPass(TravelPass& travelPass, Journey& journey, 
        bool printOut = true);
    virtual float getCostOf(TravelPass& travelPass) const;
};

#endif