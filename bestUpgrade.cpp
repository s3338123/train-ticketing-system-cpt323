#include "bestUpgrade.h"

BestUpgrade::BestUpgrade() {}

BestUpgrade::BestUpgrade(Journey &journey) : journey(journey) {}

BestUpgrade::~BestUpgrade() {}

TravelPass* BestUpgrade::operator()(set<TravelPass*>& priceList) {
    set<TravelPass*>::iterator it;
    for (it = priceList.begin(); it != priceList.end(); ++it) {
        if (journey.getZone() == 1 && (*it)->isTravelPass(AllDayZone1())) {
            return (*it)->clone();
        }
        if (journey.getZone() == 2 && (*it)->isTravelPass(AllDayZone1And2())) {
            return (*it)->clone();
        }
    }

    // Problems getting c++11 features working with uni servers
    //for (auto& pass : priceList) {
    //    if (journey.getZone() == 1 && pass->isTravelPass(AllDayZone1())) {
    //        return pass->clone();
    //    }
    //    else if (journey.getZone() == 2 && 
    //        pass->isTravelPass(AllDayZone1And2())) {
    //        return pass->clone();
    //    }
    //}

    return NULL;
}