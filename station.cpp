#include "station.h"

Station::Station() {}

Station::Station(string name, int zone) 
    : name(name), zone(zone) {}

Station::~Station() {}

istream& operator >>(istream& input, Station& station) {
    string name;
    int zone;
    getline(input, name, ':') >> zone;

    station.setName(name);
    station.setZone(zone);

    return input;
}

ostream& operator <<(ostream& output, const Station& station) {
    return output;
}

string Station::getName() const {
    return name;
}

int Station::getZone() const {
    return zone;
}

void Station::setName(string name) {
    this->name = name;
}

void Station::setZone(int zone) {
    this->zone = zone;
}