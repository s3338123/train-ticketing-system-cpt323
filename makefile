OBJS = addNewUserCommand.o allDayZone1.o allDayZone1And2.o bestMatch.o bestUpgrade.o buyJourneyCommand.o canUpgrade.o chargeCardCommand.o component.o exitMenuCommand.o fullMyTic.o function.o journey.o juniorMyTic.o menu.o menuItem.o menuNavigator.o myTic.o myTicSystem.o nullCommand.o printPurchaseReport.o printStationStats.o printUserReportsCommand.o runVisitor.o saveQuitCommand.o seniorMyTic.o showCreditCommand.o showStationStatsCommand.o station.o stationRegister.o testTravelPass.o travelPass.o travelPassFactory.o twoHoursZone1.o twoHoursZone1And2.o updatePricingCommand.o user.o userCommand.o userCommandFactory.o userFactory.o utility1.o visitor.o
CC = g++
DEBUG = -g
CFLAGS = -std=c++11 -Wall -pedantic -c $(DEBUG)
LFLAGS = -Wall -pedantic $(DEBUG)

all : Ass2

Ass2 : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o Ass2

addNewUserCommand.o : addNewUserCommand.cpp addNewUserCommand.h userCommand.h myTicSystem.h
	$(CC) $(CFLAGS) addNewUserCommand.cpp
	
allDayZone1.o : allDayZone1.cpp allDayZone1.h travelPass.h
	$(CC) $(CFLAGS) allDayZone1.cpp

allDayZone1And2.o : allDayZone1And2.cpp allDayZone1And2.h travelPass.h
	$(CC) $(CFLAGS) allDayZone1And2.cpp

bestMatch.o : bestMatch.cpp bestMatch.h function.h journey.h travelPass.h
	$(CC) $(CFLAGS) bestMatch.cpp

bestUpgrade.o : bestUpgrade.cpp bestUpgrade.h function.h journey.h travelPass.h allDayZone1.h allDayZone1And2.h
	$(CC) $(CFLAGS) bestUpgrade.cpp

buyJourneyCommand.o : buyJourneyCommand.cpp buyJourneyCommand.h userCommand.h myTicSystem.h
	$(CC) $(CFLAGS) buyJourneyCommand.cpp

canUpgrade.o : canUpgrade.cpp canUpgrade.h function.h myTic.h
	$(CC) $(CFLAGS) canUpgrade.cpp

chargeCardCommand.o : chargeCardCommand.cpp chargeCardCommand.h userCommand.h myTicSystem.h
	$(CC) $(CFLAGS) chargeCardCommand.cpp

component.o : component.cpp component.h
	$(CC) $(CFLAGS) component.cpp
	
exitMenuCommand.o : exitMenuCommand.cpp exitMenuCommand.h userCommand.h
	$(CC) $(CFLAGS) exitMenuCommand.cpp

fullMyTic.o : fullMyTic.cpp fullMyTic.h myTic.h
	$(CC) $(CFLAGS) fullMyTic.cpp

function.o : function.cpp function.h
	$(CC) $(CFLAGS) function.cpp

journey.o : journey.cpp journey.h station.h
	$(CC) $(CFLAGS) journey.cpp

juniorMyTic.o : juniorMyTic.cpp juniorMyTic.h myTic.h
	$(CC) $(CFLAGS) juniorMyTic.cpp

menu.o : menu.cpp menu.h component.h userCommand.h
	$(CC) $(CFLAGS) menu.cpp

menuItem.o : menuItem.cpp menuItem.h component.h userCommand.h
	$(CC) $(CFLAGS) menuItem.cpp
	
menuNavigator.o : menuNavigator.cpp menuNavigator.h runVisitor.h component.h
	$(CC) $(CFLAGS) menuNavigator.cpp

myTic.o : myTic.cpp myTic.h function.h utility1.h travelPass.h travelPassFactory.h allDayZone1.h allDayZone1And2.h twoHoursZone1.h twoHoursZone1And2.h journey.h bestMatch.h bestUpgrade.h printPurchaseReport.h stationRegister.h menu.h menuItem.h menuNavigator.h
	$(CC) $(CFLAGS) myTic.cpp

myTicSystem.o : myTicSystem.cpp myTicSystem.h user.h userFactory.h station.h travelPass.h journey.h canUpgrade.h printStationStats.h
	$(CC) $(CFLAGS) myTicSystem.cpp

nullCommand.o : nullCommand.cpp nullCommand.h userCommand.h
	$(CC) $(CFLAGS) nullCommand.cpp

printPurchaseReport.o : printPurchaseReport.cpp printPurchaseReport.h function.h travelPass.h
	$(CC) $(CFLAGS) printPurchaseReport.cpp

printStationStats.o : printStationStats.cpp printStationStats.h function.h station.h stationRegister.h
	$(CC) $(CFLAGS) printStationStats.cpp

printUserReportsCommand.o : printUserReportsCommand.cpp printUserReportsCommand.h userCommand.h myTicSystem.h
	$(CC) $(CFLAGS) printUserReportsCommand.cpp

runVisitor.o : runVisitor.cpp runVisitor.h menuNavigator.h visitor.h menu.h menuItem.h
	$(CC) $(CFLAGS) runVisitor.cpp

saveQuitCommand.o : saveQuitCommand.cpp saveQuitCommand.h userCommand.h myTicSystem.h
	$(CC) $(CFLAGS) saveQuitCommand.cpp

seniorMyTic.o : seniorMyTic.cpp seniorMyTic.h myTic.h
	$(CC) $(CFLAGS) seniorMyTic.cpp

showCreditCommand.o : showCreditCommand.cpp showCreditCommand.h userCommand.h myTicSystem.h
	$(CC) $(CFLAGS) showCreditCommand.cpp

showStationStatsCommand.o : showStationStatsCommand.cpp showStationStatsCommand.h userCommand.h myTicSystem.h
	$(CC) $(CFLAGS) showStationStatsCommand.cpp

station.o : station.cpp station.h
	$(CC) $(CFLAGS) station.cpp

stationRegister.o : stationRegister.cpp stationRegister.h station.h
	$(CC) $(CFLAGS) stationRegister.cpp

testTravelPass.o : testTravelPass.cpp testTravelPass.h myTic.h myTicSystem.h menu.h menuItem.h menuNavigator.h
	$(CC) $(CFLAGS) testTravelPass.cpp

travelPass.o : travelPass.cpp travelPass.h journey.h utility1.h
	$(CC) $(CFLAGS) travelPass.cpp

travelPassFactory.o : travelPassFactory.cpp travelPassFactory.h travelPass.h allDayZone1.h allDayZone1And2.h twoHoursZone1.h twoHoursZone1And2.h journey.h
	$(CC) $(CFLAGS) travelPassFactory.cpp

twoHoursZone1.o : twoHoursZone1.cpp twoHoursZone1.h travelPass.h
	$(CC) $(CFLAGS) twoHoursZone1.cpp

twoHoursZone1And2.o : twoHoursZone1And2.cpp twoHoursZone1And2.h travelPass.h
	$(CC) $(CFLAGS) twoHoursZone1And2.cpp

updatePricingCommand.o : updatePricingCommand.cpp updatePricingCommand.h userCommand.h myTicSystem.h
	$(CC) $(CFLAGS) updatePricingCommand.cpp

user.o : user.cpp user.h myTic.h
	$(CC) $(CFLAGS) user.cpp
	
userCommand.o : userCommand.cpp userCommand.h myTic.h myTicSystem.h
	$(CC) $(CFLAGS) userCommand.cpp

userCommandFactory.o : userCommandFactory.cpp userCommandFactory.h myTicSystem.h userCommand.h addNewUserCommand.h buyJourneyCommand.h chargeCardCommand.h printUserReportsCommand.h saveQuitCommand.h showCreditCommand.h showStationStatsCommand.h updatePricingCommand.h nullCommand.h
	$(CC) $(CFLAGS) userCommandFactory.cpp

userFactory.o : userFactory.cpp userFactory.h user.h fullMyTic.h juniorMyTic.h seniorMyTic.h
	$(CC) $(CFLAGS) userFactory.cpp
	
utility1.o : utility1.cpp utility1.h
	$(CC) $(CFLAGS) utility1.cpp

visitor.o : visitor.cpp visitor.h
	$(CC) $(CFLAGS) visitor.cpp

clean:
	rm -f *.o core