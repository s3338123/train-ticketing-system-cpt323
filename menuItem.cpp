#include "menuItem.h"
#include "visitor.h"

MenuItem::MenuItem(char menuChoice, string menuTitle, UserCommand& command)
: Component(menuChoice, menuTitle), command(&command) {}

MenuItem::~MenuItem() {}

int MenuItem::run() const {
    return this->command->execute();
}

Component* MenuItem::item(char choice) {
    return this;
}

void MenuItem::accept(const Visitor& v) {
    v.visit(*this);
}
