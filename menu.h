#ifndef __MENU_H__
#define __MENU_H__

#include <vector>

#include "component.h"
#include "userCommand.h"

using std::vector;

/*
Holds MenuItem objects and other Menu objects.
This class represents the Composite in the Composite pattern.
*/
class Menu : public Component {
public:
    Menu(string menuItem);
    Menu(char menuChoice, string menuItem);
    ~Menu();

    // stores a menu (composite) or menu item (leaf) in this current menu
    void add(Component& item);
    // displays the menu and its associated items
    virtual int run() const;
    // returns the menu item associated with the menu choice parameter
    virtual Component* item(char choice);
    // accepts and executes the visit method for the visitor pattern
    virtual void accept(const Visitor& v);
private:
    vector <Component*> menuItems;
};

#endif
