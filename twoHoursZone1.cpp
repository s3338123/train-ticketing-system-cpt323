#include "twoHoursZone1.h"

const string TwoHoursZone1::PASS_LENGTH = "2 Hour";
const string TwoHoursZone1::PASS_ZONES = "Zone 1";
const float TwoHoursZone1::PASS_COST = 2.50f;

TwoHoursZone1::TwoHoursZone1() 
    : TravelPass(PASS_LENGTH, PASS_ZONES, PASS_COST) {}

TwoHoursZone1::TwoHoursZone1(string theLength, string theZones, float theCost) 
    : TravelPass(theLength, theZones, theCost) {}

TwoHoursZone1::~TwoHoursZone1() {}

TravelPass* TwoHoursZone1::clone() const {
    return new TwoHoursZone1(*this);
}

int TwoHoursZone1::getExpiry() const {
    return getTimeOfPurchase() + 200;
}

bool TwoHoursZone1::isBestMatch(Journey& journey) {
    return (journey.getDuration() <= 2 && journey.getZone() == 1);
}

bool TwoHoursZone1::isValid(Journey& journey) {
    return (journey.getDay() == journeys.front()->getDay()
        && journey.getArriveTime() <= journeys.front()->getDepartTime() + 200);
}

bool TwoHoursZone1::isTravelPass(const TravelPass& travelPass) {
    return (typeid(*this) == typeid(travelPass));
}

void TwoHoursZone1::input() {
    cin >> *this;
}

void TwoHoursZone1::print() {
    cout << *this;
}