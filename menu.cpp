#include "menu.h"
#include "visitor.h"

Menu::Menu(string menuItem) : Component('0', menuItem) {}

Menu::Menu(char menuChoice, string menuItem) 
    : Component(menuChoice, menuItem) {}

Menu::~Menu() {}

void Menu::add(Component& item) {
    // If menu choice already exists, then replace the menu item
    for (unsigned i = 0; i < this->menuItems.size(); i++) {
        if (item.getChoice() == this->menuItems.at(i)->getChoice()) {
            this->menuItems.at(i) = &item;
            return;
        }
    }

    this->menuItems.push_back(&item);
}

int Menu::run() const {
    cout << this->getTitle() << endl;
    for (unsigned i = 0; i < this->menuItems.size(); i++) {
        cout << this->menuItems.at(i)->getChoice() << ". "
            << this->menuItems.at(i)->getTitle() << endl;
    }
    // for now, returning int here is unused but required by base class
    return 0;
}

Component* Menu::item(char choice) {
    for (unsigned i = 0; i < this->menuItems.size(); i++) {
        if (choice == this->menuItems.at(i)->getChoice()) {
            return this->menuItems.at(i);
        }
    }

    return 0;
}

void Menu::accept(const Visitor& v) {
    v.visit(*this);
}
