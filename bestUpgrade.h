#ifndef __BESTUPGRADE_H__
#define __BESTUPGRADE_H__

#include <set>

#include "function.h"
#include "journey.h"
#include "travelPass.h"
#include "allDayZone1.h"
#include "allDayZone1And2.h"

using std::set;

class BestUpgrade : public Function < set<TravelPass*>&, TravelPass* > {
public:
    BestUpgrade();
    BestUpgrade(Journey &journey);
    ~BestUpgrade();

    virtual TravelPass* operator() (set<TravelPass*>& priceList);
private:
    Journey journey;
};

#endif