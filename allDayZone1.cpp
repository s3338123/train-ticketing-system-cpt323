#include "allDayZone1.h"

const string AllDayZone1::PASS_LENGTH = "All Day";
const string AllDayZone1::PASS_ZONES = "Zone 1";
const float AllDayZone1::PASS_COST = 4.90f;

AllDayZone1::AllDayZone1() : TravelPass(PASS_LENGTH, PASS_ZONES, PASS_COST) {}

AllDayZone1::AllDayZone1(string theLength, string theZones, float theCost) 
    : TravelPass(theLength, theZones, theCost) {}

AllDayZone1::~AllDayZone1() {}

TravelPass* AllDayZone1::clone() const {
    return new AllDayZone1(*this);
}

int AllDayZone1::getExpiry() const {
    return 2359;
}

bool AllDayZone1::isBestMatch(Journey& journey) {
    return (journey.getDuration() > 2 && journey.getZone() == 1);
}

bool AllDayZone1::isValid(Journey& journey) {
    return journeys.front()->getDay() == journey.getDay();
}

bool AllDayZone1::isTravelPass(const TravelPass& travelPass) {
    return (typeid(*this) == typeid(travelPass));
}

void AllDayZone1::input() {
    cin >> *this;
}

void AllDayZone1::print() {
    cout << *this;
}
