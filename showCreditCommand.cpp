#include "showCreditCommand.h"

ShowCreditCommand::ShowCreditCommand(MyTicSystem &system) : system(system) {}

ShowCreditCommand::~ShowCreditCommand() {}

int ShowCreditCommand::execute() {
    system.showUserCredit();
    return 1;
}