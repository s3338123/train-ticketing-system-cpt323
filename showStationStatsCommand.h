#ifndef __SHOWSTATIONSTATSCOMMAND_H__
#define __SHOWSTATIONSTATSCOMMAND_H__

#include "userCommand.h"
#include "myTicSystem.h"

class ShowStationStatsCommand : public UserCommand {
public:
    ShowStationStatsCommand(MyTicSystem &system);
    ~ShowStationStatsCommand();

    virtual int execute();
private:
    MyTicSystem system;
};

#endif