#include "seniorMyTic.h"

const float SeniorMyTic::DISCOUNT_RATE = 0.5f;

SeniorMyTic::SeniorMyTic() {}

SeniorMyTic::SeniorMyTic(User* user) : MyTic(user) {}

SeniorMyTic::SeniorMyTic(float credit, User* user) : MyTic(credit, user) {}

SeniorMyTic::~SeniorMyTic() {}

void SeniorMyTic::buyPass(TravelPass& travelPass, Journey& journey, 
    bool printOut) {
    float concession = journey.getDay() == Journey::Day::sun 
        ? 0 : getCostOf(travelPass);
    travelPass.setCost(concession);
    MyTic::buyPass(travelPass, journey, printOut);
}

float SeniorMyTic::getCostOf(TravelPass& travelPass) const {
    return travelPass.getCost() * DISCOUNT_RATE;
}