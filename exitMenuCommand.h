#ifndef __EXITMENUCOMMAND_H__
#define __EXITMENUCOMMAND_H__

#include "userCommand.h"

/*
Encapsulates the function required to exit from the programs console menu.
This class acts as the Concrete Command in the Command pattern.
*/
class ExitMenuCommand : public UserCommand {
public:
	ExitMenuCommand();
	~ExitMenuCommand();

	int execute();
};

#endif
