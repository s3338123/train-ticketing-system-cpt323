#ifndef __MYTICSYSTEM_H__
#define __MYTICSYSTEM_H__

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <sstream>

#include "user.h"
#include "userFactory.h"
#include "station.h"
#include "travelPass.h"
#include "journey.h"
#include "canUpgrade.h"
#include "printStationStats.h"

using std::cerr;
using std::istream;
using std::map;
using std::ostream;
using std::set;

/*
Controller class which holds MyTic instances and performs the
required system functions
*/
class MyTicSystem {
public:
    template <class ITERATOR, class FUNCTION>
    inline FUNCTION 
    for_each(ITERATOR first, ITERATOR last, FUNCTION f) {
        for (; first != last; ++first) {
            f(*first);
        }
        return f;
    }

    MyTicSystem(map<string, User*>& users, set<TravelPass*>& passes, 
        map<string, Station*>& stations);
    ~MyTicSystem();

    void buyJourney();
    void buyPass(Journey& journey, MyTic& myTic);
    void rechargeCard();
    void showUserCredit();
    void printUserReports();
    void updatePricing();
    void showStationStats();
    void addNewUser();
    void saveTo(ostream& output);

private:
    map<string, User*>& users;
    set<TravelPass*> priceList;
    map<string, Station*> stations;

    Journey* getJourneyDetails();
};

#endif