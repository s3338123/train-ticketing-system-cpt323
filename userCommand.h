// UserCommand acts as an abstract class
#ifndef __USERCOMMAND_H__
#define __USERCOMMAND_H__

#include <map>

#include "myTic.h"
#include "myTicSystem.h"

using std::map;

/*
Allows encapsulation of functions for relevant program commands.
Acts as the interface in the command pattern.
This base class implements a static make function which acts as
a factory for its derived classes
*/
class UserCommand {
public:
    UserCommand();
    virtual ~UserCommand();

    // Details the command/s to be performed when this object or its derived
    // classes is executed.
    virtual int execute() = 0;
};

#endif
